﻿using System.Web;
using System.Web.Optimization;

namespace DK.Vue
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Content/jquery/jquery.min.js",
                "~/Content/bootstrap/js/bootstrap.bundle.min.js",
                "~/Scripts/lodash.min.js",
                "~/Scripts/axios.min.js",
                "~/Scripts/sb-admin.js",
                "~/Scripts/vuejs/vue.min.js",
                "~/Scripts/dk.vue.global.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //            "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap/css/bootstrap.min.css",
                      "~/Content/font-awesome/css/font-awesome.min.css",
                      "~/Content/sb-admin.css"));
        }
    }
}
