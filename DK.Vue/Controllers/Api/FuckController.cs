﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace DK.Vue.Controllers.Api
{
    public class FuckController : ApiController
    {
        static List<Fucker> data = new List<Fucker>() { new Fucker { Id = 1, Title = "Ngoc Trinh" } };

        [HttpGet]
        [Route("api/fuckers")]
        public List<Fucker> Get()
        {
            return data;
        }

        [HttpGet]
        [Route("api/fuckers/{id}")]
        public Fucker GetById(int id)
        {
            return data.FirstOrDefault(m => m.Id == id);
        }

        [HttpPost]
        [Route("api/fuckers")]
        public Fucker Add(Fucker model)
        {
            if (data.Count == 0)
                model.Id = 1;
            else
                model.Id = data.Max(m => m.Id) + 1;
            data.Add(model);
            return model;
        }

        [HttpPut]
        [Route("api/fuckers/{id}")]
        public Fucker Edit(Fucker model)
        {
            data.Remove(data.FirstOrDefault(m => m.Id == model.Id));
            data.Add(model);
            return model;
        }

        [HttpDelete]
        [Route("api/fuckers/{id}")]
        public void Delete(int id)
        {
            data.Remove(data.FirstOrDefault(m => m.Id == id));
        }
    }

    public class Fucker
    {
        public int Id { get; set; }
        public string Title { get; set; }
    }
}